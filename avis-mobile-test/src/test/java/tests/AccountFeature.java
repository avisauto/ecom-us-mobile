package tests;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.avis.qa.listeners.report.ExtentListener;

import ObjectRepository.AccountPage;
import ObjectRepository.LoginPage;
import core.Configuration;
import core.MobileInstance;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.offset.PointOption;
import utilities.CommonUtils;

@Listeners({ExtentListener.class})
public class AccountFeature extends MobileInstance {

	@Test(priority = 1)
	public void account() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 120);
		CommonUtils utils = new CommonUtils();

		System.out.println("Step 1: Click on Signin and Signup Button.");

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(LoginPage.SIGNIN_SIGNUP_ID)));
		driver.findElement(By.id(LoginPage.SIGNIN_SIGNUP_ID)).click();

		System.out.println("Step 2: Login into Application.");

		List<WebElement> OTP_PopUp = driver.findElements(By.id(LoginPage.SIX_DIGIT_OTP_ID));

		if (OTP_PopUp.size() > 0) {

			driver.findElement(By.id(LoginPage.SIX_DIGIT_OTP_ID)).click();
			driver.findElement(By.id(LoginPage.SIX_DIGIT_OTP_ID)).sendKeys("");
			System.out.println("Step 3: Enter the OTP and Click the Submit Button.");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(LoginPage.OTP_SUBMIT_BUTTON)));
			driver.findElement(By.id(LoginPage.OTP_SUBMIT_BUTTON)).click();

		} else {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(LoginPage.EMAIL_ADDRESS_ID)));
			driver.findElement(By.id(LoginPage.EMAIL_ADDRESS_ID)).click();
			driver.findElement(By.id(LoginPage.EMAIL_ADDRESS_ID)).sendKeys(Configuration.USERNAME);

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(LoginPage.PASSWORD_ID)));
			driver.findElement(By.id(LoginPage.PASSWORD_ID)).click();
			driver.findElement(By.id(LoginPage.PASSWORD_ID)).sendKeys(Configuration.PASSWORD);

			((AndroidDriver) driver).hideKeyboard();

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(LoginPage.SIGNIN_ID)));
			driver.findElement(By.id(LoginPage.SIGNIN_ID)).click();

			Thread.sleep(10000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(LoginPage.ALLOW_BTN)));
			driver.findElement(By.xpath(LoginPage.ALLOW_BTN)).click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(LoginPage.AVIS_LOGO_ID)));
		}

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(AccountPage.ACCOUNT_BOX_ID)));
		Thread.sleep(3000);
		driver.findElement(By.id(AccountPage.ACCOUNT_BOX_ID)).click();
		Thread.sleep(10000);
		new TouchAction(driver).press(PointOption.point(790, 1050)).waitAction().moveTo(PointOption.point(860, 340))
				.release().perform();
		Thread.sleep(4000);
		
		// Updating name
		driver.findElement(By.id(AccountPage.NAME_ID)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(AccountPage.FIRST_NAME_ID)));
		driver.findElement(By.id(AccountPage.FIRST_NAME_ID)).sendKeys(RandomStringUtils.randomAlphabetic(5));
		driver.findElement(By.id(AccountPage.LAST_NAME_ID)).click();
		driver.findElement(By.id(AccountPage.LAST_NAME_ID)).sendKeys(RandomStringUtils.randomAlphabetic(5));
		((AndroidDriver) driver).hideKeyboard();
		driver.findElement(By.id(AccountPage.SAVE_ID)).click();
		
		//Updating address
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(AccountPage.EDIT_PRIMARY_ADDRESS_ID)));
		driver.findElement(By.id(AccountPage.EDIT_PRIMARY_ADDRESS_ID)).click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(AccountPage.EDIT_ADDRESS_1_ID)));
		driver.findElement(By.id(AccountPage.EDIT_ADDRESS_1_ID)).click();
		driver.findElement(By.id(AccountPage.EDIT_ADDRESS_1_ID)).sendKeys(RandomStringUtils.randomAlphabetic(5));
		driver.findElement(By.id(AccountPage.EDIT_ADDRESS_2_ID)).click();
		driver.findElement(By.id(AccountPage.EDIT_ADDRESS_2_ID)).sendKeys(RandomStringUtils.randomAlphabetic(5));
		driver.findElement(By.id(AccountPage.EDIT_CITY_ID)).click();
		driver.findElement(By.id(AccountPage.EDIT_CITY_ID)).sendKeys(RandomStringUtils.randomAlphabetic(5));

		((AndroidDriver) driver).hideKeyboard();
		driver.findElement(By.id(AccountPage.SAVE_BTN_ID)).click();
		Thread.sleep(4000);
		new TouchAction(driver).press(PointOption.point(790, 1050)).waitAction().moveTo(PointOption.point(760, 240))
		 .release().perform();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(AccountPage.EDIT_PHONE_NUMBER_ID)));
		Thread.sleep(4000);
		driver.findElement(By.xpath(AccountPage.EDIT_PHONE_NUMBER_ID)).click();
		Thread.sleep(5000);
		driver.findElement(By.id(AccountPage.PHONE_NUMBER_ID)).click();
		Thread.sleep(2000);
		((AndroidDriver) driver).hideKeyboard();
		driver.findElement(By.id(AccountPage.SAVE_CHANGE_ID )).click();
		new TouchAction(driver).press(PointOption.point(790, 1050)).waitAction().moveTo(PointOption.point(760, 240))
		.release().perform();
		driver.findElement(By.id(AccountPage.DISCOUNT_CODE_ID )).click();
		Thread.sleep(2000);
		driver.findElement(By.id(AccountPage.CLOSE_ID )).click();
		Thread.sleep(3000);
		new TouchAction(driver).press(PointOption.point(790, 1050)).waitAction().moveTo(PointOption.point(760, 240))
		.release().perform();
		driver.findElement(By.id(AccountPage.CREDIT_CARD_ID )).click();
		Thread.sleep(3000);
		driver.findElement(By.id(AccountPage.CREDIT_CARD_TYPE_ID )).click();
		driver.findElement(By.xpath(AccountPage.CARD_XPATH )).click();
		driver.findElement(By.id(AccountPage.CVV_ID )).click();
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(AccountPage.CVV_ID)));
		driver.findElement(By.id(AccountPage.CVV_ID )).sendKeys(String.valueOf(1234));
		Thread.sleep(2000);
		((AndroidDriver) driver).hideKeyboard();
		driver.findElement(By.id(AccountPage.CONTINUE_BTN_ID)).click();
	}

}
