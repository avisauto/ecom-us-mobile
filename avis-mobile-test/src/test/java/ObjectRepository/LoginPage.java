package ObjectRepository;

import org.openqa.selenium.By;

public interface LoginPage {
	
	String SIGNIN_SIGNUP_ID="btn_primary";
	String EMAIL_ADDRESS_ID = "et_username";
	String PASSWORD_ID = "et_password";
	String SIGNIN_ID = "btn_login";
	String SIX_DIGIT_OTP_ID = "edt_tex_msg";
	String OTP_SUBMIT_BUTTON = "btn_submit_otp";
	String OK_DIALOG_BOX_ID = "button1";
	String SIGN_IN_BTN_ID = "btn_sign_in";
	String AVIS_LOGO_ID = "iv_logo";
	String DOTALLOW_ID = "button2";
	String ENTER_THE_PASSCODE = "ll_security_code";
	String START_YOUR_RESERVATION_HERE_BOX_ID= "cv_location_search";
	String SEARCH_FOR_A_PICK_UP_LOCATION = "edt_location_search";
	String ALLOW_BTN= "//android.widget.Button[@text='ALLOW']";

}
