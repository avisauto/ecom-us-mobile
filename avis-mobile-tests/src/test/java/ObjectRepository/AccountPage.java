package ObjectRepository;

import org.openqa.selenium.By;

public interface AccountPage {
	
	String PRIMARY_ADDRESS_ID = "tv_address";
	String ACCOUNT_BOX_ID = "action_account";
	String EDIT_PASSWORD_ID = "iv_edit_password";
	String EDIT_NAME_ID = "rl_name";
	String NAME_ID = "tv_name";
	String FIRST_NAME_ID = "et_first_name";
	String LAST_NAME_ID = "et_last_name";
	String SAVE_ID = "btn_save";
	String EDIT_PRIMARY_ADDRESS_ID = "tv_address";
	String EDIT_ADDRESS_1_ID = "et_address_1";
	String EDIT_ADDRESS_2_ID = "et_address_2";
	String EDIT_CITY_ID = "et_city";
    String SAVE_BTN_ID = "btn_save";
    String EDIT_PHONE_NUMBER_ID = "rl_phone_information";
    String PHONE_NUMBER_ID = "et_phn_number";
    String SAVE_CHANGE_ID = "btn_save";
    String DISCOUNT_CODE_ID = "tv_amz_dic_1";
    String CLOSE_ID = "menu_close";
    String CREDIT_CARD_ID = "tv_cc_number";
    String CREDIT_CARD_TYPE_ID = "ll_credit_card_type";
    String CARD_XPATH = "//android.widget.TextView[6]";
    String CVV_ID = "ll_cc_sec_number";
    String CONTINUE_BTN_ID = "btn_continue";
   
}
