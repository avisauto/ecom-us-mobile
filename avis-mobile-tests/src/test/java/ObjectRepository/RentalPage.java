package ObjectRepository;

public interface RentalPage {
	
	String RENTAL_BOX_ID = "action_rentals";
    String UPCOMING_XPATH = "//android.widget.LinearLayout[@content-desc=\"UPCOMING\"]";
    String CURRENT_XPATH = "//android.widget.LinearLayout[@content-desc=\"CURRENT\"]";
    String PAST_XPATH = "//android.widget.LinearLayout[@content-desc=\"PAST\"]";
    String RESERVE_A_CAR = "btn_primary";
    String SEARCH_FOR_A_PICK_UP_LOCATION = "edt_location_search";
  //  String 
}
