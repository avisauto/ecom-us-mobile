package ObjectRepository;
import org.openqa.selenium.By;

public interface ReserveOnewayPage
{
String RESERVE_BOX_ID = "action_reserve";
String START_YOUR_RESERVATION_HERE_BOX_ID= "rl_location_search";
String SEARCH_FOR_A_PICK_UP_LOCATION_ID = "edt_location_search";
String CANCEL_ID = "iv_close";
String LOCATION_ID = "tv_location_name";
String ONEWAY_XPATH = "//android.widget.LinearLayout[@content-desc=\"ONE WAY\"]";
String EDIT_LOCATION_ID = "txt_pickup_edit";
String CLOSE_ID = "menu_close";
String RETURN_LOCATION_EDIT_ID = "txt_return_edit";
String BACK_ID = "menu_close";
String SELECT_DATE_TIME_ID = "txt_view_confirm_pick_up_date";
String CLOSEPOPUP_ID = "iv_close";
String DATE1_XPATH = "(//*[@text='23'])[1]"; 
String SET_ID ="btn_continue";
String COUPON_CODE_ID = "ll_coupon_confirm";
String CONTINUE_ID = "btn_continue_selection_confirm";
}