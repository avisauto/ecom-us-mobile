package tests;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.avis.qa.listeners.report.ExtentListener;

import ObjectRepository.AccountPage;
import ObjectRepository.LoginPage;
import ObjectRepository.ReservePage;
import core.Configuration;
import core.MobileInstance;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.offset.PointOption;
import utilities.CommonUtils;

@Listeners({ExtentListener.class})
public class ReserveFeature extends MobileInstance {

	@Test(priority = 1)
	public void reserve() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 120);
		CommonUtils utils = new CommonUtils();

		System.out.println("Step 1: Click on Signin and Signup Button.");

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(LoginPage.SIGNIN_SIGNUP_ID)));
		driver.findElement(By.id(LoginPage.SIGNIN_SIGNUP_ID)).click();

		System.out.println("Step 2: Login into Application.");

		List<WebElement> OTP_PopUp = driver.findElements(By.id(LoginPage.SIX_DIGIT_OTP_ID));

		if (OTP_PopUp.size() > 0) {

			driver.findElement(By.id(LoginPage.SIX_DIGIT_OTP_ID)).click();
			driver.findElement(By.id(LoginPage.SIX_DIGIT_OTP_ID)).sendKeys("");
			System.out.println("Step 3: Enter the OTP and Click the Submit Button.");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(LoginPage.OTP_SUBMIT_BUTTON)));
			driver.findElement(By.id(LoginPage.OTP_SUBMIT_BUTTON)).click();

		} else {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(LoginPage.EMAIL_ADDRESS_ID)));
			driver.findElement(By.id(LoginPage.EMAIL_ADDRESS_ID)).click();
			driver.findElement(By.id(LoginPage.EMAIL_ADDRESS_ID)).sendKeys(Configuration.USERNAME);

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(LoginPage.PASSWORD_ID)));
			driver.findElement(By.id(LoginPage.PASSWORD_ID)).click();
			driver.findElement(By.id(LoginPage.PASSWORD_ID)).sendKeys(Configuration.PASSWORD);

			((AndroidDriver) driver).hideKeyboard();

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(LoginPage.SIGNIN_ID)));
			driver.findElement(By.id(LoginPage.SIGNIN_ID)).click();

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(LoginPage.AVIS_LOGO_ID)));
		}

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(ReservePage.RESERVE_BOX_ID)));
		Thread.sleep(3000);
		driver.findElement(By.id(ReservePage. RESERVE_BOX_ID)).click();
		Thread.sleep(3000);
		driver.findElement(By.id(ReservePage.START_YOUR_RESERVATION_HERE_BOX_ID)).click();
		Thread.sleep(3000);

		driver.findElement(By.id(ReservePage.CANCEL_ID)).click();
		driver.findElement(By.id(ReservePage.SEARCH_FOR_A_PICK_UP_LOCATION_ID)).click();
		driver.findElement(By.id(ReservePage.SEARCH_FOR_A_PICK_UP_LOCATION_ID)).sendKeys("ICT");
		((AndroidDriver) driver).hideKeyboard();
		Thread.sleep(3000);
		driver.findElement(By.id(ReservePage.LOCATION_ID)).click();
		Thread.sleep(3000);
		driver.findElement(By.id(ReservePage.LOCATION1_ID)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(ReservePage.EDIT_PICKUP_ID)));
		driver.findElement(By.id(ReservePage.EDIT_PICKUP_ID)).click();
		Thread.sleep(10000);
		driver.findElement(By.id(ReservePage.CANCEL1_ID)).click();
		driver.findElement(By.id(ReservePage.SELECT_TIME_DATE1_ID)).click();
		Thread.sleep(2000);
		driver.findElement(By.id(ReservePage.CLOSEPOPUP1_ID)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(ReservePage.DATE_XPATH)));
		driver.findElement(By.xpath(ReservePage.DATE_XPATH)).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath(ReservePage.DATE_XPATH)).click();
		Thread.sleep(3000);
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(ReservePage.TIME_ID)));
		driver.findElement(By.id(ReservePage.TIME_ID)).click();
		driver.findElement(By.id(ReservePage.EDIT_TIME_ID)).click();
		Thread.sleep(3000);
		new TouchAction(driver).press(PointOption.point(500, 790)).waitAction().moveTo(PointOption.point(340, 150));
		Thread.sleep(5000);
		driver.findElement(By.id(ReservePage.SET1_ID)).click();
		new TouchAction(driver).press(PointOption.point(790, 1050)).waitAction().moveTo(PointOption.point(760, 240));
		driver.findElement(By.id(ReservePage.COUPON_CODE1_ID)).click();
		driver.findElement(By.id(ReservePage.COUPON_CODE1_ID)).sendKeys("MUZZ032");
		((AndroidDriver) driver).hideKeyboard();
		driver.findElement(By.id(ReservePage.CONTINUE1_ID)).click();
		
	}

}
