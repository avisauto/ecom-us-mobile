package tests;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.avis.qa.listeners.report.ExtentListener;

import ObjectRepository.AccountPage;
import ObjectRepository.LoginPage;
import ObjectRepository.ReserveOnewayPage;
import ObjectRepository.ReservePage;
import core.Configuration;
import core.MobileInstance;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.offset.PointOption;
import utilities.CommonUtils;

@Listeners({ExtentListener.class})
public class ReserveOneway extends MobileInstance {

	@Test(priority = 1)
	public void reserveOneWay() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 120);
		CommonUtils utils = new CommonUtils();

		System.out.println("Step 1: Click on Signin and Signup Button.");

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(LoginPage.SIGNIN_SIGNUP_ID)));
		driver.findElement(By.id(LoginPage.SIGNIN_SIGNUP_ID)).click();

		System.out.println("Step 2: Login into Application.");

		List<WebElement> OTP_PopUp = driver.findElements(By.id(LoginPage.SIX_DIGIT_OTP_ID));

		if (OTP_PopUp.size() > 0) {

			driver.findElement(By.id(LoginPage.SIX_DIGIT_OTP_ID)).click();
			driver.findElement(By.id(LoginPage.SIX_DIGIT_OTP_ID)).sendKeys("");
			System.out.println("Step 3: Enter the OTP and Click the Submit Button.");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(LoginPage.OTP_SUBMIT_BUTTON)));
			driver.findElement(By.id(LoginPage.OTP_SUBMIT_BUTTON)).click();

		} else {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(LoginPage.EMAIL_ADDRESS_ID)));
			driver.findElement(By.id(LoginPage.EMAIL_ADDRESS_ID)).click();
			driver.findElement(By.id(LoginPage.EMAIL_ADDRESS_ID)).sendKeys(Configuration.USERNAME);

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(LoginPage.PASSWORD_ID)));
			driver.findElement(By.id(LoginPage.PASSWORD_ID)).click();
			driver.findElement(By.id(LoginPage.PASSWORD_ID)).sendKeys(Configuration.PASSWORD);

			((AndroidDriver) driver).hideKeyboard();

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(LoginPage.SIGNIN_ID)));
			driver.findElement(By.id(LoginPage.SIGNIN_ID)).click();

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(LoginPage.AVIS_LOGO_ID)));
		}

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(ReservePage.RESERVE_BOX_ID)));
		Thread.sleep(3000);
		driver.findElement(By.id(ReservePage. RESERVE_BOX_ID)).click();
		Thread.sleep(3000);
		driver.findElement(By.id(ReservePage.START_YOUR_RESERVATION_HERE_BOX_ID)).click();
		Thread.sleep(3000);

		driver.findElement(By.id(ReservePage.CANCEL_ID)).click();
		driver.findElement(By.id(ReservePage.SEARCH_FOR_A_PICK_UP_LOCATION_ID)).click();
		driver.findElement(By.id(ReservePage.SEARCH_FOR_A_PICK_UP_LOCATION_ID)).sendKeys("ICT");
		((AndroidDriver) driver).hideKeyboard();
		Thread.sleep(3000);
		driver.findElement(By.id(ReserveOnewayPage.LOCATION_ID)).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath(ReserveOnewayPage.ONEWAY_XPATH)).click();
		Thread.sleep(3000);
		driver.findElement(By.id(ReserveOnewayPage.EDIT_LOCATION_ID)).click();
		Thread.sleep(3000);
	//	driver.findElement(By.id(ReservePage.EDIT_LOCATION_ID)).sendKeys(RandomStringUtils.randomAlphabetic(5));
		driver.findElement(By.id(ReserveOnewayPage.CLOSE_ID)).click();
		Thread.sleep(3000);
		driver.findElement(By.id(ReserveOnewayPage.RETURN_LOCATION_EDIT_ID)).click();
		Thread.sleep(2000);
//		driver.findElement(By.id(ReservePage.RETURN_LOCATION_EDIT_ID)).sendKeys("xfgrtds");
		driver.findElement(By.id(ReserveOnewayPage.BACK_ID)).click();
		Thread.sleep(2000);
		driver.findElement(By.id(ReserveOnewayPage.SELECT_DATE_TIME_ID)).click();
		Thread.sleep(2000);
		driver.findElement(By.id(ReserveOnewayPage.CLOSEPOPUP_ID)).click();
		driver.findElement(By.xpath(ReserveOnewayPage.DATE1_XPATH)).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath(ReserveOnewayPage.DATE1_XPATH)).click();
		Thread.sleep(2000);
		driver.findElement(By.id(ReserveOnewayPage.SET_ID)).click();
		Thread.sleep(2000);
		new TouchAction(driver).press(PointOption.point(790, 1050)).waitAction().moveTo(PointOption.point(760, 240))
		.release().perform();
		driver.findElement(By.id(ReserveOnewayPage.COUPON_CODE_ID)).click();
	//	driver.findElement(By.id(ReserveOnewayPage.COUPON_CODE_ID)).sendKeys("MUZZO32");
		Thread.sleep(4000);
		((AndroidDriver) driver).hideKeyboard();
		driver.findElement(By.id(ReserveOnewayPage.CONTINUE_ID)).click();
	}
}

		