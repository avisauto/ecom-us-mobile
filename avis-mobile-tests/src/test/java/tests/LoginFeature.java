package tests;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.avis.qa.listeners.report.ExtentListener;

import ObjectRepository.AccountPage;
import ObjectRepository.LoginPage;
import core.Configuration;
import core.MobileInstance;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.offset.PointOption;
import utilities.CommonUtils;
@Listeners({ExtentListener.class})
public class LoginFeature extends MobileInstance {

	@Test(priority = 1)
	public void login() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 120);
		CommonUtils utils = new CommonUtils();
	


		System.out.println("Step 1: Click on Signin and Signup Button.");

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(LoginPage.SIGNIN_SIGNUP_ID)));
		driver.findElement(By.id(LoginPage.SIGNIN_SIGNUP_ID)).click();

		System.out.println("Step 2: Login into Application.");
		

		List<WebElement> OTP_PopUp = driver.findElements(By.id(LoginPage.SIX_DIGIT_OTP_ID));
		
	    if(OTP_PopUp.size()>0) {
	    	
			driver.findElement(By.id(LoginPage.SIX_DIGIT_OTP_ID)).click();
			driver.findElement(By.id(LoginPage.SIX_DIGIT_OTP_ID)).sendKeys("");
			System.out.println("Step 3: Enter the OTP and Click the Submit Button.");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(LoginPage.OTP_SUBMIT_BUTTON)));
			driver.findElement(By.id(LoginPage.OTP_SUBMIT_BUTTON)).click();
			
		}else {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(LoginPage.EMAIL_ADDRESS_ID)));
		driver.findElement(By.id(LoginPage.EMAIL_ADDRESS_ID)).click();
		driver.findElement(By.id(LoginPage.EMAIL_ADDRESS_ID)).sendKeys(Configuration.USERNAME);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(LoginPage.PASSWORD_ID)));
		driver.findElement(By.id(LoginPage.PASSWORD_ID)).click();
		driver.findElement(By.id(LoginPage.PASSWORD_ID)).sendKeys(Configuration.PASSWORD);

		((AndroidDriver) driver).hideKeyboard();

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(LoginPage.SIGNIN_ID)));
		driver.findElement(By.id(LoginPage.SIGNIN_ID)).click();

//		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(LoginPage.AVIS_LOGO_ID)));
		}
//	    driver.switchTo().alert().dismiss();
	    wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(LoginPage.START_YOUR_RESERVATION_HERE_BOX_ID)));
//	    driver.findElement(By.id(LoginPage. START_YOUR_RESERVATION_HERE_BOX_ID)).click(); 
//	    driver.findElement(By.id(LoginPage.SEARCH_FOR_A_PICK_UP_LOCATION)).click(); 
	}

}

