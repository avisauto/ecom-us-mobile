package com.avis.qa.core;

import io.github.bonigarcia.wdm.WebDriverManager;
import lombok.extern.log4j.Log4j2;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.safari.SafariDriver;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Class Contains local driver setup
 *
 * @author ikumar
 */
@Log4j2
public class BrowserInstance {

    private String url;
    protected WebDriver webDriver;
    protected String browser;

    public BrowserInstance(String browser) {
        this.browser = browser;
    }

    private void initializeDriver() {


//        log.info("Initializing browser: " + browser);
        switch (browser.toLowerCase()) {
            case "chrome":
                initializeChrome();
                break;
            case "firefox":
                initializeFirefox();
                break;
            case "edge":
                initializeEdge();
                break;
            case "safari":
                initializeSafari();
                break;
            case "pixel":
                initializeMobileEmulation("Pixel 2");
                break;
            case "iphone":
                initializeMobileEmulation("iPhone X");
                break;
            case "ipad":
                initializeMobileEmulation("iPad Pro");
                break;
            default:
                throw new RuntimeException("Invalid Browser Input. Valid Browsers are chrome, firefox, safari, edge, pixel, iphone, ipad");
        }
    }

    private void initializeChrome() {
        WebDriverManager.chromedriver().setup();
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setAcceptInsecureCerts(true);

        webDriver = new ChromeDriver(chromeOptions);
    }

    private void initializeFirefox() {
        WebDriverManager.firefoxdriver().setup();
        FirefoxOptions firefoxOptions = new FirefoxOptions();
        firefoxOptions.setAcceptInsecureCerts(true);

        webDriver = new FirefoxDriver(firefoxOptions);
    }

    private void initializeEdge() {
        WebDriverManager.edgedriver().setup();
        webDriver = new EdgeDriver();
    }

    private void initializeMobileEmulation(String deviceName){
        Map<String, String> mobileEmulation = new HashMap<>();
        mobileEmulation.put("deviceName", deviceName);

        WebDriverManager.chromedriver().setup();
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulation);

        webDriver = new ChromeDriver(chromeOptions);
    }

    /**
     * INFO: Parallel tests is not possible locally on Safari due to restriction
     * https://developer.apple.com/documentation/webkit/about_webdriver_for_safari
     */
    private void initializeSafari() {
        if (Configuration.getValue("os.name").toLowerCase().contains("mac")) {
            webDriver = new SafariDriver();
        } else {
            throw new RuntimeException("Safari is supported only on Mac Operating System.");
        }
    }

    /**
     * Configures the driver instance
     */
    protected void configureDriver() {
//        log.info("Maximizing Browser and Setting Implicit Wait Timeout to :" + Configuration.DEFAULT_IMPLICIT_TIMEOUT);
        webDriver.manage().deleteAllCookies();
        webDriver.manage().window().maximize();
        webDriver.manage().timeouts().implicitlyWait(Configuration.DEFAULT_IMPLICIT_TIMEOUT, TimeUnit.SECONDS);
    }

    public void load(String url) {
//        log.info("Launching URL: " + url);
        webDriver.get(url);
    }

    public WebDriver getDriver() {
        return webDriver;
    }


    public void start(String url) {
        initializeDriver();
        configureDriver();
        load(url);
    }
}
