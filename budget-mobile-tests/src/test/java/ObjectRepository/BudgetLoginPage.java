package ObjectRepository;

import org.openqa.selenium.By;

public interface BudgetLoginPage

{
	String SIGN_IN_XPATH = "//android.widget.TextView[@text='Sign In']";
	String USERNAME_XPATH = "//android.widget.EditText[@text='Enter username or customer ID']";
	String PASSWORD_XPATH = "//android.widget.EditText[@text='Enter your password']";
	String LOGIN_XPATH = "//android.widget.Button[@text='Log In']";
	String START_YOUR_RESERVATION_HERE_XPATH = "//android.widget.TextView[@text='Start your reservation here']";
	String POPUP_XPATH = "//android.widget.TextView[3]";
	String SEARCH_FOR_A_PICKUP_LOCATION_XPATH = "//android.widget.EditText";
	String CLICK_LOCATION_XPATH = "(//android.widget.ImageView[@content-desc=\"More Info\"])[1]";
	String SELECT_LOCATION_XPATH = "//android.widget.Button[@text='Select Location']";
	String LOCATION_EDIT_XPATH ="//android.widget.TextView[@text='Pick up & Return']";
	String BACK_XPATH = "//android.widget.ImageButton[@content-desc=\"Back\"]";
	String SELECT_DATE_AND_TIME_XPATH = "//android.widget.TextView[@text='Pick Up']";
	String GOT_IT_BTN_XPATH = "//android.widget.TextView[@text='Got It!']";
	String DATE_XPATH = "//android.widget.CheckedTextView[24]" ;
	String SET_TIME_AND_DATES_XPATH = "//android.widget.Button[@text='Set Dates And Times']";
//	String COUPON_CODE_XPATH = "//android.widget.EditText[@text='Enter a coupon code']";
//	String CONTINUE_XPATH = "//android.widget.Button[@text='Continue']";
	      //ONEWAY TRIP//
	   
	String ONEWAY_TRIP_XPATH = "//android.widget.TextView[@text='One Way']";
	String EDIT_LOCATION_XPATH = "(//android.widget.ImageView[@content-desc=\"Edit\"])[1]";
	String BACK1_XPATH = "//android.widget.ImageButton[@content-desc=\"Back\"]";
	String EDIT_LOCATION1_XPATH = "(//android.widget.ImageView[@content-desc=\"Edit\"])[2]";
	String BACK2_XPATH =  "//android.widget.ImageButton[@content-desc=\"Back\"]";
	String SELECT_DATE_TIME_XPATH = "(//android.widget.TextView[@text='Pick Up'])[1]";
	String GOT_IT_BTN2_XPATH = "//android.widget.TextView[3]";
	String DATE1_XPATH = "(//*[@text='23'])[1]" ;
	String SET1_TIME_AND_DATES_XPATH = "//android.widget.Button[@text='Set Dates And Times']";
	String COUPON_CODE_XPATH = "//android.widget.EditText[@text='Enter a coupon code']";
	String CONTINUE_XPATH = "//android.widget.Button[@text='Continue']";
}
