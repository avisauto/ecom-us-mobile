package ObjectRepository;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

public interface BudgetWithoutLoginFeaturePage
{

	String START_YOUR_RESERVATION_HERE_XPATH = "//android.widget.TextView[@text='Start your reservation here']";
	String SEARCH_YUOR_LOCATION_XPATH = "//android.widget.EditText[@text='Search for a pick up location']";
	String LOCATION_XPATH = "(//android.widget.ImageView[@content-desc=\"More Info\"])[1]";
	String SELECT_LOCATION_XPATH = "//android.widget.Button[@text='Select Location']";
	String PICKUP_AND_RETURN_XPATH = "//android.widget.TextView[@text='Edit']";
	String BACK_XPATH = "//android.widget.ImageButton[@content-desc=\"Back\"]";
	String EDIT_PICKUP_XPATH = "//android.widget.TextView[@text='Pick Up']";
	String GOT_IT_BTN_XPATH = "//android.widget.TextView[@text='Got It!']";
	String DATE_XPATH = "//android.widget.CheckedTextView[24]" ;
	String SET_TIME_XPATH = "//android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.FrameLayout[1]";
	String SET_TIME_AND_DATES_XPATH = "//android.widget.Button[@text='Set Dates And Times']";
	String ONEWAY_TRIP_XPATH = "//android.widget.TextView[@text='One Way']";
	String EDIT_LOCATION_XPATH = "(//android.widget.ImageView[@content-desc=\"Edit\"])[1]";
	String BACK1_XPATH = "//android.widget.ImageButton[@content-desc=\"Back\"]";
	String EDIT_LOCATION1_XPATH = "(//android.widget.ImageView[@content-desc=\"Edit\"])[2]";
	String BACK2_XPATH =  "//android.widget.ImageButton[@content-desc=\"Back\"]";
	String SELECT_DATE_TIME_XPATH = "(//android.widget.TextView[@text='Pick Up'])[1]";
	String GOT_IT_BTN2_XPATH = "//android.widget.TextView[3]";
	String DATE1_XPATH = "(//*[@text='23'])[1]" ;
	String SET1_TIME_AND_DATES_XPATH = "//android.widget.Button[@text='Set Dates And Times']";
//	String COUPON_CODE_XPATH = "//android.widget.EditText[@text='Enter a coupon code']";
    String SELECT_AGE_XPATH = "(//android.widget.ImageView[@content-desc=\"SELECT\"])[1]";
    String AGE_XPATH = "//android.widget.TextView[3]";
    String SELECT_RECEDANCY_XPATH = "(//android.widget.ImageView[@content-desc=\"SELECT\"])[2]";
    String RECEDANCY_XPATH = "//android.widget.TextView[2]";
    String CONTINUE_XPATH = "//android.widget.Button[@text='Continue']";
}