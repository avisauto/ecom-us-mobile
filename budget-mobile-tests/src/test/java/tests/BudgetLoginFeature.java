package tests;


import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import com.avis.qa.listeners.report.ExtentListener;
import ObjectRepository.BudgetLoginPage;
import core.Configuration;
import core.MobileInstance;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.offset.PointOption;
import utilities.CommonUtils;
@Listeners({ExtentListener.class})
public class BudgetLoginFeature extends MobileInstance {

	@Test(priority = 1)
	public void login() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 120);
		CommonUtils utils = new CommonUtils();
	


		System.out.println("Step 1: Click on Signin Button.");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BudgetLoginPage.SIGN_IN_XPATH)));
		driver.findElement(By.xpath(BudgetLoginPage.SIGN_IN_XPATH)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BudgetLoginPage.USERNAME_XPATH)));
		System.out.println("Step 2: Login into Application.");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BudgetLoginPage.USERNAME_XPATH)));
		driver.findElement(By.xpath(BudgetLoginPage.USERNAME_XPATH)).click();
		driver.findElement(By.xpath(BudgetLoginPage.USERNAME_XPATH)).sendKeys(Configuration.USERNAME);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BudgetLoginPage.PASSWORD_XPATH)));
		driver.findElement(By.xpath(BudgetLoginPage.PASSWORD_XPATH)).click();
		driver.findElement(By.xpath(BudgetLoginPage.PASSWORD_XPATH)).sendKeys(Configuration.PASSWORD);
		Thread.sleep(3000);
		driver.findElement(By.xpath(BudgetLoginPage.LOGIN_XPATH)).click();
		Thread.sleep(5000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BudgetLoginPage.START_YOUR_RESERVATION_HERE_XPATH)));
		driver.findElement(By.xpath(BudgetLoginPage.START_YOUR_RESERVATION_HERE_XPATH)).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath(BudgetLoginPage.POPUP_XPATH )).click();
		driver.findElement(By.xpath(BudgetLoginPage.SEARCH_FOR_A_PICKUP_LOCATION_XPATH )).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath(BudgetLoginPage.SEARCH_FOR_A_PICKUP_LOCATION_XPATH )).sendKeys("new york");
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BudgetLoginPage.CLICK_LOCATION_XPATH)));
		driver.findElement(By.xpath(BudgetLoginPage.CLICK_LOCATION_XPATH )).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath(BudgetLoginPage.SELECT_LOCATION_XPATH )).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BudgetLoginPage.LOCATION_EDIT_XPATH)));
		driver.findElement(By.xpath(BudgetLoginPage.LOCATION_EDIT_XPATH )).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath(BudgetLoginPage.BACK_XPATH )).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath(BudgetLoginPage.SELECT_DATE_AND_TIME_XPATH )).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath(BudgetLoginPage.GOT_IT_BTN_XPATH )).click();
		//driver.findElement(By.xpath(BudgetLoginPage.SELECT_LOCATION_XPATH )).click();
		System.out.println("******************");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BudgetLoginPage.DATE_XPATH)));
		driver.findElement(By.xpath(BudgetLoginPage.DATE_XPATH )).click();
		driver.findElement(By.xpath(BudgetLoginPage.DATE_XPATH )).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BudgetLoginPage.SET_TIME_AND_DATES_XPATH)));
		driver.findElement(By.xpath(BudgetLoginPage.SET_TIME_AND_DATES_XPATH )).click();
	//	Thread.sleep(5000);
	//	driver.findElement(By.xpath(BudgetLoginPage.COUPON_CODE_XPATH )).click();
	//	driver.findElement(By.xpath(BudgetLoginPage.COUPON_CODE_XPATH )).sendKeys("MUZZ032");
	//	((AndroidDriver) driver).hideKeyboard();
		//	driver.findElement(By.xpath(BudgetLoginPage.CONTINUE_XPATH )).click();
		      //Oneway trip////
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BudgetLoginPage.ONEWAY_TRIP_XPATH )));
		driver.findElement(By.xpath(BudgetLoginPage.ONEWAY_TRIP_XPATH )).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath(BudgetLoginPage.EDIT_LOCATION_XPATH  )).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath(BudgetLoginPage. BACK1_XPATH  )).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath(BudgetLoginPage.EDIT_LOCATION1_XPATH  )).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath(BudgetLoginPage. BACK2_XPATH  )).click();
		Thread.sleep(5000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BudgetLoginPage.SELECT_DATE_TIME_XPATH )));
		driver.findElement(By.xpath(BudgetLoginPage. SELECT_DATE_TIME_XPATH  )).click();
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BudgetLoginPage.GOT_IT_BTN2_XPATH )));
		driver.findElement(By.xpath(BudgetLoginPage.GOT_IT_BTN2_XPATH )).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath(BudgetLoginPage.COUPON_CODE_XPATH )).click();
		driver.findElement(By.xpath(BudgetLoginPage.COUPON_CODE_XPATH )).sendKeys("MUZZ032");
		((AndroidDriver) driver).hideKeyboard();
		driver.findElement(By.xpath(BudgetLoginPage.CONTINUE_XPATH )).click();
		
		}
}