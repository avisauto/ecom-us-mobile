package tests;


import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import com.avis.qa.listeners.report.ExtentListener;
import ObjectRepository.BudgetLoginPage;
import ObjectRepository.BudgetWithoutLoginFeaturePage;
import core.MobileInstance;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.offset.PointOption;
import utilities.CommonUtils;
@Listeners({ExtentListener.class})
public class BudgetWithoutLoginFeature extends MobileInstance {

	@Test(priority = 1)
	public void Withoutlogin() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 120);
		CommonUtils utils = new CommonUtils();
		System.out.println("start");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BudgetWithoutLoginFeaturePage.START_YOUR_RESERVATION_HERE_XPATH )));
		driver.findElement(By.xpath(BudgetWithoutLoginFeaturePage.START_YOUR_RESERVATION_HERE_XPATH)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BudgetWithoutLoginFeaturePage.GOT_IT_BTN_XPATH )));
		driver.findElement(By.xpath(BudgetWithoutLoginFeaturePage.GOT_IT_BTN_XPATH)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BudgetWithoutLoginFeaturePage.SEARCH_YUOR_LOCATION_XPATH)));
		driver.findElement(By.xpath(BudgetWithoutLoginFeaturePage.SEARCH_YUOR_LOCATION_XPATH)).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath(BudgetWithoutLoginFeaturePage.SEARCH_YUOR_LOCATION_XPATH)).sendKeys("ict");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BudgetWithoutLoginFeaturePage.LOCATION_XPATH)));
		driver.findElement(By.xpath(BudgetWithoutLoginFeaturePage.LOCATION_XPATH )).click();
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BudgetWithoutLoginFeaturePage.SELECT_LOCATION_XPATH)));
		driver.findElement(By.xpath(BudgetWithoutLoginFeaturePage.SELECT_LOCATION_XPATH )).click();
		Thread.sleep(4000);
		driver.findElement(By.xpath(BudgetWithoutLoginFeaturePage.PICKUP_AND_RETURN_XPATH )).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath(BudgetWithoutLoginFeaturePage.BACK_XPATH)).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath(BudgetWithoutLoginFeaturePage.EDIT_PICKUP_XPATH)).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath(BudgetWithoutLoginFeaturePage.GOT_IT_BTN_XPATH )).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BudgetWithoutLoginFeaturePage.DATE_XPATH)));
		driver.findElement(By.xpath(BudgetWithoutLoginFeaturePage.DATE_XPATH )).click();
		driver.findElement(By.xpath(BudgetWithoutLoginFeaturePage.DATE_XPATH )).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath(BudgetWithoutLoginFeaturePage.SET_TIME_XPATH )).click();
		Thread.sleep(2000);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		HashMap<String, String> scrollObject = new HashMap<String, String>();
		scrollObject.put("direction", "down");
		js.executeScript("mobile: scroll", scrollObject);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BudgetWithoutLoginFeaturePage.SET_TIME_AND_DATES_XPATH)));
		driver.findElement(By.xpath(BudgetWithoutLoginFeaturePage.SET_TIME_AND_DATES_XPATH )).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BudgetWithoutLoginFeaturePage.ONEWAY_TRIP_XPATH )));
		driver.findElement(By.xpath(BudgetWithoutLoginFeaturePage.ONEWAY_TRIP_XPATH )).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath(BudgetWithoutLoginFeaturePage.EDIT_LOCATION_XPATH  )).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath(BudgetWithoutLoginFeaturePage. BACK1_XPATH  )).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath(BudgetWithoutLoginFeaturePage.EDIT_LOCATION1_XPATH  )).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath(BudgetWithoutLoginFeaturePage. BACK2_XPATH  )).click();
		Thread.sleep(5000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BudgetLoginPage.SELECT_DATE_TIME_XPATH )));
		driver.findElement(By.xpath(BudgetWithoutLoginFeaturePage. SELECT_DATE_TIME_XPATH  )).click();
		
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BudgetWithoutLoginFeaturePage.GOT_IT_BTN2_XPATH )));
		driver.findElement(By.xpath(BudgetWithoutLoginFeaturePage.GOT_IT_BTN2_XPATH )).click();
		Thread.sleep(1000);
		
		
		driver.findElement(By.xpath(BudgetWithoutLoginFeaturePage.SELECT_AGE_XPATH  )).click();
		driver.findElement(By.xpath(BudgetWithoutLoginFeaturePage.AGE_XPATH  )).click();
		Thread.sleep(300);
		driver.findElement(By.xpath(BudgetWithoutLoginFeaturePage.SELECT_RECEDANCY_XPATH )).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath(BudgetWithoutLoginFeaturePage.RECEDANCY_XPATH )).click();
		Thread.sleep(3000);
//		driver.findElement(By.xpath(BudgetWithoutLoginFeaturePage.COUPON_CODE_XPATH )).click();
//		driver.findElement(By.xpath(BudgetWithoutLoginFeaturePage.COUPON_CODE_XPATH )).sendKeys("MUZZ032");
//		((AndroidDriver) driver).hideKeyboard();
		driver.findElement(By.xpath(BudgetWithoutLoginFeaturePage.CONTINUE_XPATH )).click();
		
	}
}
	