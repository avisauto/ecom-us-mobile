package core;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;

import com.avis.qa.listeners.report.*;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;


public class MobileInstance {
	protected static AppiumDriver driver;

	@BeforeClass
	public void setUp() throws MalformedURLException {
		DesiredCapabilities Mobile = new DesiredCapabilities();
		Mobile.setCapability("deviceName", "Pixel");
		Mobile.setCapability("platformName", "Android");
		//Mobile.setCapability("automationName", "uiAutomator2");
		Mobile.setCapability("udid", "emulator-5554");
		Mobile.setCapability("platformVersion", "10.0");
		Mobile.setCapability("app", "C:\\Users\\Lenovo\\Downloads\\Budget_Automation_QA_Endpoint_v8_3.apk");
		Mobile.setCapability("autoGrantPermissions", "true");
        Mobile.setCapability("appActivity", "com.androidapp.budget.views.activities.SplashActivity");
        Mobile.setCapability("appPackage", "com.budget.androidapp");
	    driver = new AndroidDriver(new URL("http://localhost:4723/wd/hub"), Mobile);
	    ExtentListener.extent = ExtentManager.createInstance();
	    
	}
	
	@AfterClass
	public  void close(){
		driver.quit();
	}	
}