package utilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Random;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.touch.TouchActions;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class CommonUtils  {
    // Sleep time
    public static final long ONE_SECOND = 1000;
    public static final long TWO_SECONDS = 2000;
    public static final long THREE_SECONDS = 3000;
    /**
     * Returns the random number between the min and max number
     *
     * @param min minimum range 0
     * @param max maximum int range
     * @return random value
     */
    public static String getRandomNumber(int min, int max) {
        Random random = new Random();
        int number = random.nextInt((max - min) + 1) + min;
        return String.valueOf(number);
    }
    
    public static CharSequence[] getRandomString() {
    	 byte[] array = new byte[7]; // length is bounded by 7
    	    new Random().nextBytes(array);
    	    String generatedString = new String(array, Charset.forName("UTF-8"));

    	    System.out.println(generatedString);
			return null;
    }
    /**
     * This method will pause the thread execution for specified time
     *
     * @param time Time(in milliseconds) till when the thread execution will be paused
     */
    public static void threadSleep(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
          //  log.info("There was an error while wait for " + time + " milliseconds. Please try again.");
        }
    }
    public ArrayList<Object[]> intialDriver(String a, String b) throws IOException {
    	ArrayList<Object[]> mydata=new ArrayList<Object[]>();
    	Properties pro=new Properties();
    	FileInputStream fis=new FileInputStream("C:\\Users\\Lenovo\\Desktop\\Framework\\avistest\\avis-mobile-tests\\config.properties");
    	pro.load(fis);
    	String userName=pro.getProperty(a);
    	String passWord=pro.getProperty(b);
    	Object ob[]=new Object[] {userName, passWord};
    	mydata.add(ob);
    	return mydata;
    	
    	
    }
    }
